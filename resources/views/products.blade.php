<!DOCTYPE html>
<html>
<head>
<title>Mogaka Skills Test</title>
	<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    {{--jQuery--}}
    <script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
</head>

<body>
	<div class="container">
		<div class="row">
	
			<div class="col-md-12">
				<h1>Coalition Technologies Skills Test</h1>

				<h3>Input Products</h3>
					<form>
						<input type="hidden" name="_token" value="{{csrf_token()}}">
						<input type="text" name="name" id="name" placeholder="Product Name" required>
						<input type="number" name="qty_in_stock" id="qty_in_stock" placeholder="Quantity In Stock" min="0" required>
						<input type="number" name="price" id="price" placeholder="Price Per Item" min="0" required>
						<input type="button" value="Add Product" onclick="submitProduct()" class="btn btn-primary">
					</form>
				<hr>

				<table class="table" id="products_table">
					<thead>
						<tr>
							<th>Product Name</th>
							<th>Qty In Stock</th>
							<th>Price / Item</th>
							<th>Submitted</th>
							<th>Total Value Number</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						@if ($products->count())
							@foreach ($products as $product)
								<tr>
									<td>{{$product->name}}</td>
									<td>{{$product->qty_in_stock}}</td>
									<td>${{$product->price}}</td>
									<td>{{$product->created_at->format('m/d/Y')}}</td>
									<td>{{$product->total_value_number}}</td>
									<td><button type="button" class="btn btn-primary">Update (TO DO)</button></td>
								</tr>
								<div id="new_product"></div>
							@endforeach
							<tr>
								<td></td>
								<td></td>
								<td></td>
								<td>Total</td>
								<td id="total_sum">{{$products->sum('total_value_number')}}</td>
							</tr>
						@else
							<tr>
								<td> No Products Found</td>
							</tr>
						@endif
					</tbody>
				</table>			
			</div>
	
		</div>
	</div>
	
</body>
<script type="text/javascript">
	function submitProduct(){
		var name = document.getElementById('name').value;
		var qty_in_stock = document.getElementById('qty_in_stock').value;
		var price = document.getElementById('price').value;

		$.ajax({
               	type:'POST',
               	url:"{{route('products.store')}}",
               	data:{
               		_token: "<?php echo csrf_token() ?>", 
               		name: name,
               		qty_in_stock: qty_in_stock,
               		price: price,
               	},
               	success:function(data){
                	//Looked up how to dynamically create a row
                	//Ref: https://www.w3schools.com/jsref/met_table_insertrow.asp
                	
                	var table = document.getElementById('products_table');
                	var row = table.insertRow(1);
                	var name_cell = row.insertCell(0);
					var qty_cell = row.insertCell(1);
					var price_cell = row.insertCell(2);
					var submitted_cell = row.insertCell(3);
					var total_cell = row.insertCell(4);
					var update_cell = row.insertCell(5);
					name_cell.innerHTML = data.name;
					qty_cell.innerHTML = data.qty_in_stock;
					price_cell.innerHTML = data.price;
					submitted_cell.innerHTML = data.submitted;
					total_cell.innerHTML = data.total_value_number;
					update_cell.innerHTML = 'UPDATE';

					//Update Total Sum
					var new_sum = Number(document.getElementById('total_sum').innerHTML) + data.total_value_number;
					document.getElementById('total_sum').innerHTML = new_sum;
               	}
            });
	}
</script>

</html>