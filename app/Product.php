<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id' 
    ];

    /**
     * Accessor to return Total Value Number
     * @return string 
     */
    public function getTotalValueNumberAttribute(){
    	return $this->qty_in_stock * $this->price;
    }
}
